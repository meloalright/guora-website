import * as moment from 'moment';
import 'moment/locale/zh-cn';
import 'moment/locale/en-SG';

export default unixtime => moment(unixtime * 1000).calendar();
