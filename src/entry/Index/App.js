import React, { Component } from 'react';
import styled from 'styled-components';

import BasePageCombination from 'component/Combinations/PageCombination/BasePageCombination';
import { ContentColumn } from 'style/Layout';
import Page from './Page';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.combinationRef = React.createRef();
    this.handleClickAskLink = () => {
      this.combinationRef.current.toggle();
    };
  }
  render() {
    return [
      <BasePageCombination ref={this.combinationRef} key="combination" />,
      <AppCenterRoot key="app">
        <Page onClickAskLink={this.handleClickAskLink} />
      </AppCenterRoot>
    ];
  }
}

export default App;
