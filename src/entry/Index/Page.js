/* eslint-disable no-script-url */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Card from 'widget/Card';
import LinkTitle from 'component/LinkTitle';
import SmallProfileBar from 'component/ProfileBars/SmallProfileBar';
import MeService from 'service/MeService';
import AnswerList from 'component/Lists/AnswerList';
import HotQuestion from 'component/HotQuestion';
import { DashboardRoot, MainColumnRoot, SideColumnRoot } from 'style/DashboardColumnStyled';

const CSR = window.$CSRDATA;
const CSRHotAnswers = CSR.hotAnswers;
const CSRHotAnswersCounts = CSR.hotAnswersCounts;
const CSRHotQuestions = CSR.hotQuestions;

const TopCard = styled(Card)`
  padding: 20px;
  margin-bottom: 10px;
`;

const PostLinkTitle = styled(LinkTitle)`
  display: inline-block;
  line-height: 20px;
  padding-top: 8px;
`;

const Page = props => {
  const { onClickAskLink } = props;
  return (
    <DashboardRoot>
      <MainColumnRoot>
        <TopCard>
          <SmallProfileBar data={MeService.GetMe()} />
          <PostLinkTitle href="javascript: void(0)" onClick={onClickAskLink}>
            <FormattedMessage id="index.post.question" />
          </PostLinkTitle>
        </TopCard>
        <Card>
          <AnswerList
            from="index"
            id={-1}
            data={{ list: CSRHotAnswers, total: CSRHotAnswersCounts }}
          />
        </Card>
      </MainColumnRoot>
      <SideColumnRoot>
        <HotQuestion data={CSRHotQuestions} />
      </SideColumnRoot>
    </DashboardRoot>
  );
};

Page.propTypes = {
  onClickAskLink: PropTypes.func.isRequired
};

export default Page;
