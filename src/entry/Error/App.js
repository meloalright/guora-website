import React, { Component } from 'react';
import styled from 'styled-components';

import LogoNavigator from 'component/Navigators/LogoNavigator';
import { ContentColumn } from 'style/Layout';
import Page from './Page';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

class App extends Component {
  render() {
    return [
      <LogoNavigator key="nav" />,
      <AppCenterRoot key="app">
        <Page />
      </AppCenterRoot>
    ];
  }
}

export default App;
