import React, { Component } from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import { URL } from 'service/URLService';
import { Red, LightRed } from 'style/Color';

const ErrorCode = styled.code`
  color: ${Red};
  display: block;
  font-size: 12px;
  padding: 5px 10px;
  border-radius: 2px;
  white-space: pre-wrap;
  background-color: ${LightRed};
`;

const Container = styled.div`
  margin: 100px;
  cursor: pointer;
`;

const Message = URL.search('message') || '';

class Page extends Component {
  render() {
    return [
      <Container
        onClick={() => {
          URL.back();
        }}
      >
        <ErrorCode>
          <FormattedMessage key="error" id="error" />
          {`: ${Message} `}
          <FormattedMessage key="contact" id="contact.admin" />
        </ErrorCode>
      </Container>
    ];
  }
}

export default Page;
