import React from 'react';
import styled from 'styled-components';

import BasePageCombination from 'component/Combinations/PageCombination/BasePageCombination';
import { ContentColumn } from 'style/Layout';
import Page from './Page';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

const App = () => {
  return [
    <BasePageCombination key="combination" />,
    <AppCenterRoot key="app">
      <Page />
    </AppCenterRoot>
  ];
};

export default App;
