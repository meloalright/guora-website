import React, { Component } from 'react';
import styled from 'styled-components';
import HotQuestion from 'component/HotQuestion';
import { DashboardRoot, MainColumnRoot, SideColumnRoot } from 'style/DashboardColumnStyled';
import QuestionCard from 'component/QuestionCard';
import AnswerList from 'component/Lists/AnswerList';
import AnswerQuestionCard from 'component/AnswerQuestionCard';
import Card from 'widget/Card';

const CSR = window.$CSRDATA;
const CSRQuestion = CSR.question;
const CSRHotQuestions = CSR.hotQuestions;
const CSRAnswers = CSR.answers;
const CSRAnswersCounts = CSR.answersCounts;

const Container = styled.div`
  margin-top: 16px;
`;

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: false
    };

    this.showAnswer = () => {
      let { answer } = this.state;
      if (answer) {
        return;
      }
      answer = true;
      this.setState({ answer });
    };
  }

  render() {
    const { answer } = this.state;

    return (
      <DashboardRoot>
        <MainColumnRoot>
          <QuestionCard data={CSRQuestion} onClickAnswer={this.showAnswer} />
          {answer && (
            <Container>
              <AnswerQuestionCard questionID={CSRQuestion.id} />
            </Container>
          )}
          <Container>
            <Card style={{ marginTop: 16 }}>
              <AnswerList
                from="question"
                id={CSRQuestion.id}
                data={{ list: CSRAnswers, total: CSRAnswersCounts }}
                expand
              />
            </Card>
          </Container>
        </MainColumnRoot>
        <SideColumnRoot>
          <HotQuestion data={CSRHotQuestions} />
        </SideColumnRoot>
      </DashboardRoot>
    );
  }
}

export default Page;
