/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import styled from 'styled-components';

import { ContentColumn } from 'style/Layout';
import Page from './Page';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

class App extends Component {
  render() {
    return (
      <AppCenterRoot key="app">
        <Page />
      </AppCenterRoot>
    );
  }
}

export default App;
