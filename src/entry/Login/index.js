import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider } from 'react-intl';
import * as moment from 'moment';
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from 'serviceWorker';
import 'style/normalize.css';
import 'style/default.css';
import 'style/animation.css';

import zh from 'locale/zh';
import en from 'locale/en';
import ToastService from 'service/ToastService';
import App from './App';
import './index.css';

ToastService.init();

const messages = {
  en,
  zh
};

const lng = document.documentElement.lang || 'en';

moment.locale({ en: 'en-SG', zh: 'zh-cn' }[lng]);

ReactDOM.render(
  <IntlProvider locale={lng} messages={messages[lng]}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </IntlProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
