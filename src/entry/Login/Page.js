/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import styled from 'styled-components';
import { CenterColumnRoot } from 'style/CenterColumnStyled';
import Login from 'component/Logins/Login';

const Wrap = styled.div`
  display: flex;
  justify-content: center;
`;

class Page extends Component {
  render() {
    return (
      <CenterColumnRoot>
        <Wrap>
          <Login />
        </Wrap>
      </CenterColumnRoot>
    );
  }
}

export default Page;
