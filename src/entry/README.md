# Entry

## Quick Create

`App.js`

```js
import React, { Component } from 'react';
import styled from 'styled-components';

import Navigator from 'component/Navigators/Navigator';
import { ContentColumn } from 'style/Layout';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

const H1 = styled.h1`
  margin: 100px;
`;

class App extends Component {
  render() {
    return [
      <Navigator key="nav" />,
      <AppCenterRoot key="app">
        <H1>Index</H1>
      </AppCenterRoot>
    ];
  }
}

export default App;
```

`App.test.js`

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
```

`index.js`

```js
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from 'serviceWorker';
import 'style/normalize.css';
import 'style/default.css';
import 'style/animation.css';

import ToastService from 'service/ToastService';

import App from './App';

ToastService.init();

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
```
