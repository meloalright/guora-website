import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import HotQuestion from 'component/HotQuestion';
import { DashboardRoot, MainColumnRoot, SideColumnRoot } from 'style/DashboardColumnStyled';
import AnswerList from 'component/Lists/AnswerList';
import { SkyBlue, HoverSkyBlue } from 'style/Color';
import Link from 'widget/Link';
import { URL } from 'service/URLService';
import { FiChevronRight } from 'react-icons/fi';

import Card from 'widget/Card';

const CSR = window.$CSRDATA;
const CSRHotQuestions = CSR.hotQuestions;
const CSRAnswer = CSR.answer;
const CSRAnswersCounts = CSR.answersCounts;

const Container = styled.div``;

const ViewAllContianer = styled(Link)`
  display: flex;
  padding: 10px 0;
  cursor: pointer;
  font-size: 12px;
  justify-content: center;
  background-color: ${SkyBlue};
  :hover {
    background-color: ${HoverSkyBlue};
  }
`;

const Span = styled.span`
  justify-content: center;
  align-items: center;
  display: inline-flex;
`;

const Page = () => {
  return (
    <DashboardRoot>
      <MainColumnRoot>
        <Container>
          <Card>
            <AnswerList
              from="answer"
              id={CSRAnswer.questionID}
              data={{ list: [CSRAnswer], total: CSRAnswersCounts }}
            />
            <ViewAllContianer href={URL.question(CSRAnswer.questionID)}>
              <FormattedMessage id="view.all.answer" />
              <Span>
                <FiChevronRight />
              </Span>
            </ViewAllContianer>
          </Card>
        </Container>
      </MainColumnRoot>
      <SideColumnRoot>
        <HotQuestion data={CSRHotQuestions} />
      </SideColumnRoot>
    </DashboardRoot>
  );
};

export default Page;
