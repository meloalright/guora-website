import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Search from 'component/Search';
import Pagination from './Pagination';

const Root = styled.div`
  padding: 10px 10px 0;
  position: relative;
  display: flex;
  z-index: 20;
`;

const SearchContainer = styled.div`
  width: 180px;
`;

const SelectContainer = styled.div`
  margin-left: 10px;
`;

const TableFilter = ({ Q, P, counts, onSearch, onPagination }) => {
  const pageNum = +P;
  const pageTotal = Math.ceil(counts / 10) || 1;
  return (
    <Root>
      <SearchContainer>
        <FormattedMessage id="admin.query">
          {([txt]) => <Search placeholder={txt} type="number" query={Q} onSearch={onSearch} />}
        </FormattedMessage>
      </SearchContainer>
      <SelectContainer>
        <Pagination
          pageNum={pageNum}
          pageTotal={pageTotal}
          lock={Q.length > 0}
          onPagination={onPagination}
          onReset={() => onPagination('1')}
        />
      </SelectContainer>
    </Root>
  );
};

TableFilter.propTypes = {
  onSearch: PropTypes.func.isRequired,
  onPagination: PropTypes.func.isRequired,
  Q: PropTypes.string.isRequired,
  P: PropTypes.string.isRequired,
  counts: PropTypes.number.isRequired
};

export default TableFilter;
