import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import MeService from 'service/MeService';
import { URL } from 'service/URLService';

import { White, HoverWhite } from 'style/Color';
import { Bold } from 'style/Weight';
import Card from 'widget/Card';
import { FiLogOut, FiChevronRight } from 'react-icons/fi';
import { GoGear } from 'react-icons/go';
import { MdPerson } from 'react-icons/md';
import LogoutService from 'service/RequestServices/LogoutService';

const Root = styled(Card)`
  background-color: ${White};
`;

const AvatarRoot = styled.div`
  display: flex;
  padding: 15px 0;
  align-items: center;
  justify-content: center;
`;

const AvatarWrap = styled.div`
  width: 120px;
  height: 120px;
  cursor: pointer;
`;

const ListItem = styled.div`
  display: flex;
  padding: 15px;
  cursor: pointer;
  justify-content: left;
  :hover {
    background-color: ${HoverWhite};
  }
`;

const Span = styled.span`
  justify-content: center;
  align-items: center;
  display: inline-flex;
`;

const P = styled.p`
  padding: 0 10px;
  font-size: 12px;
  white-space: nowrap;
  font-weight: ${Bold};
`;

const Holder = styled.div`
  flex: 1;
`;

const ProfileBubble = () => {
  const handleLogout = () => {
    LogoutService.logout().then(data => {
      if (data === 1) {
        URL.go(URL.login());
      }
    });
  };

  return (
    <Root>
      <AvatarRoot>
        <AvatarWrap onClick={() => URL.go(URL.profile(MeService.GetMe().id))}>
          {MeService.AvatarComponent({
            asLink: false
          })}
        </AvatarWrap>
      </AvatarRoot>
      <ListItem onClick={() => URL.go(URL.profile(MeService.GetMe().id))}>
        <Span>
          <MdPerson />
        </Span>

        <P>
          <FormattedMessage id="navigator.profile" />
        </P>
        <Holder />
        <Span>
          <FiChevronRight />
        </Span>
      </ListItem>
      {MeService.GetMe().type === 100 && (
        <ListItem onClick={() => URL.go(URL.admin())}>
          <Span>
            <GoGear />
          </Span>
          <P>
            <FormattedMessage id="navigator.admin" />
          </P>
          <Holder />
          <Span>
            <FiChevronRight />
          </Span>
        </ListItem>
      )}
      <ListItem onClick={handleLogout}>
        <Span>
          <FiLogOut />
        </Span>
        <P>
          <FormattedMessage id="navigator.logout" />
        </P>
      </ListItem>
    </Root>
  );
};

export default ProfileBubble;
