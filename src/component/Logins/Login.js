import React, { Component } from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import Button from 'widget/Button';
import Image from 'widget/Image';

import LoadingPage from 'widget/LoadingPage';
import { Black, Blue, GrayLight4 } from 'style/Color';
import { URL } from 'service/URLService';
import LoginService from 'service/RequestServices/LoginService';
import Card from 'widget/Card';

const LoginCard = styled(Card)`
  align-items: center;
  position: relative;
  display: flex;
  height: 280px;
  width: 400px;
`;

const Wrap = styled.div`
  width: 100%;
  padding: 24px;
`;

const SloganContainer = styled.div`
  text-align: center;
  height: 30px;
`;

const InputColumn = styled.div`
  margin-top: 24px;
`;

const Input = styled.input`
  transition: border-bottom-color 0.2s ease-in-out;
  border-bottom-color: ${GrayLight4};
  border-bottom-style: solid;
  border-bottom-width: 1px;
  border-right: none;
  border-left: none;
  border-top: none;
  line-height: 20px;
  overflow: hidden;
  padding: 10px 0;
  font-size: 14px;
  color: ${Black};
  width: 100%;
  :focus {
    border-bottom-color: ${Blue};
  }
`;

const SubmitWrap = styled.div`
  margin-top: 24px;
  text-align: center;
`;

const SubmitButton = styled(Button)`
  width: 100%;
  text-align: center;
  display: inline-block;
`;

const DefaultMail = '';
const DefaultPassword = '';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      mail: DefaultMail,
      password: DefaultPassword
    };
    this.handleChangeMail = this.handleChangeMail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeMail(event) {
    this.setState({ mail: event.target.value });
  }

  handleChangePassword(event) {
    this.setState({ password: event.target.value });
  }

  handleSubmit() {
    const { mail, password } = this.state;
    let { loading } = this.state;
    loading = true;
    this.setState({ loading });
    LoginService.login({ mail, password })
      .then(data => {
        if (data === 1) {
          URL.go(URL.index());
        } else {
          Promise.reject(data);
        }
      })
      .catch(() => {
        loading = false;
        this.setState({ loading });
      });
  }

  render() {
    const { mail, password, loading } = this.state;
    const isValid = mail.length > 0 && password.length > 0;
    return (
      <LoginCard>
        {loading && <LoadingPage />}
        <Wrap style={{ visibility: loading ? 'hidden' : 'visible' }}>
          <SloganContainer>
            <Image src="/static/assets/guora.svg" />
          </SloganContainer>
          <InputColumn>
            <FormattedMessage id="email.placeholder">
              {([txt]) => (
                <Input
                  name="mail"
                  placeholder={txt}
                  onChange={this.handleChangeMail}
                  defaultValue={DefaultMail}
                />
              )}
            </FormattedMessage>
          </InputColumn>
          <InputColumn>
            <FormattedMessage id="password.placeholder">
              {([txt]) => (
                <Input
                  name="password"
                  defaultValue={DefaultPassword}
                  placeholder={txt}
                  type="password"
                  onChange={this.handleChangePassword}
                  onKeyPress={e => {
                    if (e.nativeEvent.keyCode === 13 && isValid) {
                      this.handleSubmit();
                    }
                  }}
                />
              )}
            </FormattedMessage>
          </InputColumn>
          <SubmitWrap>
            <SubmitButton
              disabled={!isValid}
              type="submit"
              theme="primary"
              onClick={() => isValid && this.handleSubmit()}
            >
              <FormattedMessage id="login" />
            </SubmitButton>
          </SubmitWrap>
        </Wrap>
      </LoginCard>
    );
  }
}

export default Login;
