import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Black, GrayLight3, Blue } from 'style/Color';
import ContainerWithBottomLine from 'widget/ContainerWithBottomLine';
import { Boldest, Lightest } from 'style/Weight';

const ProfileTabRoot = styled(ContainerWithBottomLine)`
  display: flex;
  justify-content: left;
`;

const TabRoot = styled.div`
  cursor: pointer;
  font-size: 16px;
  padding: 0 20px;
  box-sizing: border-box;
  &.focus a {
    font-weight: ${Boldest};
    border-bottom: solid 6px ${Blue};
  }
`;

const A = styled.a`
  box-sizing: border-box;
  display: inline-block;
  line-height: 50px;
  font-size: 16px;
  color: ${Black};
  height: 50px;
`;

const Span = styled.span`
  font-size: 14px;
  margin-left: 6px;
  font-weight: ${Lightest};
  color: ${GrayLight3};
`;

class ProfileTab extends Component {
  constructor(props) {
    super(props);
    this.state = { focus: props.focus };
  }

  switchTab(index) {
    const focus = index;
    this.setState({ focus });
    this.props.onSelect(index);
  }

  render() {
    const { focus } = this.state;

    const { data } = this.props;

    const list = [
      {
        id: 0,
        type: 'answers',
        num: data.answersCounts
      },
      {
        id: 1,
        type: 'questions',
        num: data.questionsCounts
      },
      {
        id: 2,
        type: 'documents',
        num: 0
      }
    ];

    return (
      <ProfileTabRoot bottomLine>
        {list.map(tab => {
          return (
            <Tab key={tab.id} focus={focus === tab.id} onSwitch={() => this.switchTab(tab.id)}>
              <A>
                <FormattedMessage id={`profile.tab.${tab.type}`} />
                <Span>{tab.num}</Span>
              </A>
            </Tab>
          );
        })}
      </ProfileTabRoot>
    );
  }
}

const Tab = props => {
  return (
    <TabRoot className={props.focus && 'focus'} onClick={props.onSwitch}>
      {props.children}
    </TabRoot>
  );
};

ProfileTab.propTypes = {
  onSelect: PropTypes.func.isRequired,
  focus: PropTypes.number.isRequired,
  data: PropTypes.shape({
    answersCounts: PropTypes.number.isRequired,
    questionsCounts: PropTypes.number.isRequired
  }).isRequired
};

Tab.propTypes = {
  focus: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onSwitch: PropTypes.func.isRequired
};

export default ProfileTab;
