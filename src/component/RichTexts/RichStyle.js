// Custom overrides for "code" style.
export const styleMap = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  }
};

export function getBlockStyle(block) {
  switch (block.getType()) {
    case 'blockquote':
      return 'Rich-blockquote';
    default:
      return null;
  }
}
