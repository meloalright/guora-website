import styled from 'styled-components';
import { White, AlmostWhite, Black, GrayLight2, GrayLight3, Gray, BrightBlue } from 'style/Color';

const RichRoot = styled.div`
  background: ${White};
  line-height: 1.6;
  font-size: 15px;
  color: ${Black};

  .Rich-blockquote {
    border-left: 5px solid #eee;
    color: ${Gray};
    font-style: italic;
    margin: 16px 0;
    padding: 10px 20px;
  }

  .public-DraftStyleDefault-pre {
    background-color: rgba(0, 0, 0, 0.05);
    overflow: scroll;
    font-size: 15px;
    padding: 20px;
  }
`;

export const RichEditorRoot = styled(RichRoot)`
  border: 1px solid #ddd;

  .Rich-editor {
    padding: 15px;
    border-top: 1px solid #ddd;
    outline: none;
    cursor: text;
    font-size: 15px;
  }

  .Rich-editor .public-DraftEditorPlaceholder-root {
    position: absolute;
    color: ${GrayLight2};
  }

  .Rich-editor .public-DraftEditor-content {
    min-height: 200px;
  }

  .Rich-hidePlaceholder .public-DraftEditorPlaceholder-root {
    display: none;
  }
`;

export const RichRenderRoot = styled(RichRoot)`
  .Rich-render {
    cursor: text;
    font-size: 15px;
  }
`;

// Controls

export const RichControl = styled.div`
  height: 51px;
  font-size: 14px;
  overflow: hidden;
  user-select: none;
`;

const ControlColumn = styled.div`
  transition: all 0.3s ease-out;
  padding: 10px 15px 10px 15px;
  box-sizing: border-box;
  display: flex;
  height: 51px;
`;

export const RichControlColumn = styled(ControlColumn)`
  background: ${AlmostWhite};
  .rich-button {
    color: ${GrayLight2};
    display: inline-flex;
    align-items: center;
    cursor: pointer;
    margin-right: 16px;
    padding: 2px 0;

    &.rich-active-button {
      color: ${BrightBlue};
    }

    &.rich-disabled-button {
      cursor: not-allowed;
      color: ${GrayLight3};
    }
  }
`;

export const LinkControlColumn = styled(ControlColumn)`
  background: ${White};
  input {
    background-color: transparent;
    margin-right: 16px;
    font-size: 14px;
    color: ${Black};
    border: none;
    flex: 1;
  }
`;
