import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MeService from 'service/MeService';
import { GrayLight2, GrayLight4 } from 'style/Color';

const me = MeService.GetMe();

const P = styled.p`
  background-color: ${GrayLight4};
  color: ${GrayLight2};
  border-radius: 4px;
  padding: 8px 16px;
  line-height: 18px;
  font-size: 13px;
  display: block;
`;

const PopupCard = props => (
  <P>
    {`${me.name}: `}
    {props.children}
  </P>
);

PopupCard.propTypes = {
  children: PropTypes.node.isRequired
};

export default PopupCard;
