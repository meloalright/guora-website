/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import PageTitle from 'widget/PageTitle';
import { White, Gray, GrayLight1 } from 'style/Color';
import Card from 'widget/Card';
import Button from 'widget/Button';
import { FaUserPlus, FaPencilAlt, FaShareAlt, FaEllipsisH } from 'react-icons/fa';
import RichTextCollapse from './RichTexts/RichTextCollapse';

const QuestionCardRoot = styled(Card)`
  padding: 20px;
`;

const Badge = styled.div`
  display: flex;
  margin: 16px 0 0 -8px;
`;

const Padding = styled.div`
  padding: 0 4px;
`;

const FaSpan = styled.span`
  font-size: 17px;
  margin-right: 8px;
  align-items: center;
  display: inline-flex;
`;

const Holder = styled.div`
  flex: 1;
`;

const IconButton = styled.button`
  background-color: ${White};
  display: inline-flex;
  align-items: center;
  line-height: 32px;
  cursor: pointer;
  font-size: 13px;
  padding: 0 7px;
  color: ${Gray};
  border: none;
  :hover {
    color: ${GrayLight1};
  }
`;

class QuestionCard extends Component {
  constructor(props) {
    super(props);
    this.f = () => {
      console.info('f');
    };
  }

  render() {
    const { data } = this.props;
    return (
      <QuestionCardRoot bottomLine>
        <PageTitle style={{ margin: '16px 0' }}>{data.title}</PageTitle>
        <RichTextCollapse data={data.desc} />
        <Badge>
          <Padding>
            <Button theme="normal" bold onClick={this.props.onClickAnswer}>
              <FaSpan>
                <FaPencilAlt />
              </FaSpan>
              <FormattedMessage id="write.answer" />
            </Button>
          </Padding>
          <Holder />
        </Badge>
      </QuestionCardRoot>
    );
  }
}

QuestionCard.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired
  }).isRequired,
  onClickAnswer: PropTypes.func.isRequired
};

export default QuestionCard;
