/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Gray } from 'style/Color';
import Avatar from 'component/Avatar';
import SmallLinkProfile from 'component/LinkProfiles/SmallLinkProfile';
import { URL } from 'service/URLService';

const ProfileBarRoot = styled.div`
  display: flex;
`;

const Circle = styled.div`
  border-radius: 50%;
  overflow: hidden;
  width: 32px;
  height: 32px;
`;

const Bar = styled.div`
  margin-left: 12px;
  flex: 1;
`;

const Badge = styled.div`
  color: ${Gray};
  line-height: 0;
  font-size: 13px;
`;

const Span = styled.span`
  color: inherit;
  font-size: 12px;
  line-height: 20px;
`;

class SmallProfileBar extends Component {
  render() {
    const { data, to } = this.props;
    return (
      <ProfileBarRoot>
        <Circle>
          <Avatar data={data} />
        </Circle>
        <Bar>
          <Badge>
            <SmallLinkProfile href={URL.profile(data.id)}>{data.name}</SmallLinkProfile>
            {to && [
              ' ',
              <FormattedMessage key="format" id="replied.to" />,
              ' ',
              <SmallLinkProfile key="name" href={URL.profile(to.id)}>
                {to.name}
              </SmallLinkProfile>
            ]}
          </Badge>
          <Badge>
            <Span>{data.desc}</Span>
          </Badge>
        </Bar>
      </ProfileBarRoot>
    );
  }
}

SmallProfileBar.defaultProps = {
  to: null
};

SmallProfileBar.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired
  }).isRequired,
  to: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired
  })
};

export default SmallProfileBar;
