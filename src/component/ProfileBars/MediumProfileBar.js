/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Gray } from 'style/Color';
import Avatar from 'component/Avatar';
import MediumLinkProfile from 'component/LinkProfiles/MediumLinkProfile';
import { URL } from 'service/URLService';

const ProfileBarRoot = styled.div`
  display: flex;
`;

const Circle = styled.div`
  border-radius: 50%;
  overflow: hidden;
  width: 40px;
  height: 40px;
`;

const Bar = styled.div`
  margin-left: 8px;
  flex: 1;
`;

const Badge = styled.div`
  line-height: 0;
`;

const Span = styled.span`
  color: ${Gray};
  font-size: 14px;
  line-height: 20px;
`;

class MediumProfileBar extends Component {
  render() {
    const { data } = this.props;
    const link = URL.profile(data.id);
    return (
      <ProfileBarRoot>
        <Circle>
          <Avatar data={data} />
        </Circle>
        <Bar>
          <Badge>
            <MediumLinkProfile href={link}>{data.name}</MediumLinkProfile>
          </Badge>
          <Badge>
            <Span>{data.desc}</Span>
          </Badge>
        </Bar>
      </ProfileBarRoot>
    );
  }
}

MediumProfileBar.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired
  }).isRequired
};

export default MediumProfileBar;
