import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ReplySocialBar from 'component/SocialBars/ReplySocialBar';
import SmallProfileBar from 'component/ProfileBars/SmallProfileBar';
import Text from 'component/Text';

const Root = styled.div``;

const Container = styled.div`
  margin-top: 8px;
`;

class ReplyItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      injected: false
    };
    this.inject = this.inject.bind(this);
    this.suck = this.suck.bind(this);
  }

  inject() {
    let { injected } = this.state;
    if (injected) {
      return;
    }
    injected = true;
    this.setState({ injected });
  }

  suck() {
    let { injected } = this.state;
    injected = false;
    this.setState({ injected });
  }

  render() {
    const { data, injectComponent } = this.props;
    const { injected } = this.state;

    return (
      <Root>
        <SmallProfileBar data={data.profile} to={data.to} />
        <Container>
          <Text>{data.content}</Text>
        </Container>
        <ReplySocialBar id={data.id} data={data.social} onClickReply={this.inject} />
        {injected && injectComponent({ onBlur: this.suck })}
      </Root>
    );
  }
}

ReplyItem.propTypes = {
  data: PropTypes.shape({
    profile: PropTypes.object.isRequired,
    content: PropTypes.string.isRequired
  }).isRequired,
  injectComponent: PropTypes.func.isRequired
};

export default ReplyItem;
