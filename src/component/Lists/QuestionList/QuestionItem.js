/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GrayLight1 } from 'style/Color';
import LinkTitle from 'component/LinkTitle';
import { URL } from 'service/URLService';
import Time from 'util/Time';

const QuestionItemRoot = styled.div``;

const Info = styled.div`
  margin-top: 5px;
`;

const Span = styled.span`
  font-size: 14px;
  line-height: 20px;
  color: ${GrayLight1};
`;

const DotSpan = styled(Span)`
  ::before {
    margin: 0 5px;
    content: '·';
  }
`;

class QuestionItem extends Component {
  render() {
    const { data } = this.props;
    const link = URL.question(data.id);

    return (
      <QuestionItemRoot>
        <LinkTitle href={link}>{data.title}</LinkTitle>
        <Info>
          <Span>{Time(data.time)}</Span>
          <DotSpan>
            {data.answersCounts} <FormattedMessage id="answers" />
          </DotSpan>
        </Info>
      </QuestionItemRoot>
    );
  }
}

QuestionItem.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    time: PropTypes.number.isRequired,
    answersCounts: PropTypes.number.isRequired
  }).isRequired
};

export default QuestionItem;
