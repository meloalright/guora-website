import styled from 'styled-components';
import { GrayLight1 } from 'style/Color';
import { Bold } from 'style/Weight';

export default styled.p`
  display: block;
  cursor: pointer;
  font-size: 12px;
  line-height: 20px;
  text-align: ${props => props.align || 'left'};
  color: ${GrayLight1};
  font-weight: ${Bold};
`;
