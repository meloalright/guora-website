import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { ShadowWhite } from 'style/Color';
import Loading from 'widget/Loading';
import ReplyList from 'component/Lists/ReplyList';
import RestService from 'service/RequestServices/RestService';
import CommentCard from 'component/TextCards/CommentCard';
import ReplyCard from 'component/TextCards/ReplyCard';
import Time from 'util/Time';
import CommentItem from './CommentItem';
import More from '../More';

const Root = styled.div`
  position: relative;
`;

const Wrap = styled.div`
  display: block;
  padding: 16px;
  background-color: ${ShadowWhite};
  position: relative;
  min-height: 12px;
`;

class CommentList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      list: []
    };
    this.handleClickMore = this.handleClickMore.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData(offset = 0) {
    const { id } = this.props;
    const limit = 10;

    RestService.fetchRestList('comments', limit, offset, `answerID=${id}`)
      .then(message => {
        const list = message;
        this.state.loading = false;
        this.state.list = this.state.list.concat(list);
        this.setState(this.state);
      })
      .catch(err => {
        console.warn(err);
      });
  }

  fetchMore() {
    const { list } = this.state;
    const offset = list.length;
    this.fetchData(offset);
  }

  handleClickMore() {
    this.state.loading = true;
    this.fetchMore();
    this.setState(this.state);
  }

  render() {
    const { loading, list } = this.state;
    const { total } = this.props;

    const more = total - list.length;
    const hasMore = more > 0;

    return (
      <Root {...this.props}>
        <Wrap>
          {(!loading || list) && <CommentCard answerID={this.props.id} />}
          {list.map(item => (
            <CommentItem
              key={item.id}
              data={{
                id: item.id,
                content: item.content,
                profile: {
                  id: item.commentProfileID,
                  name: item.commentProfile.name,
                  desc: Time(item.createAt)
                },
                social: {
                  repliesCounts: item.repliesCounts
                }
              }}
              injectComponent={[
                attr => (
                  <ReplyCard
                    commentID={item.id}
                    to={{
                      id: item.commentProfileID,
                      name: item.commentProfile.name
                    }}
                    {...attr}
                  />
                ),
                attr => <ReplyList id={item.id} {...attr} total={item.repliesCounts} />
              ]}
            />
          ))}
          {!loading && hasMore && (
            <More onClick={this.handleClickMore}>
              <FormattedMessage id="list.comment.more" />({more})
            </More>
          )}
          {loading && <Loading />}
        </Wrap>
      </Root>
    );
  }
}

CommentList.propTypes = {
  id: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired
};

export default CommentList;
