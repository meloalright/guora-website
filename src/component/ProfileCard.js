import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from 'widget/Card';
import { Black, White, Blue, StoneWhite, ModalColor } from 'style/Color';
import { FaCamera } from 'react-icons/fa';
import Avatar from './Avatar';

const ProfileCardRoot = styled(Card)``;

const ProfileBgContainer = styled.div`
  background-color: ${Blue};
  height: 198px;
`;

const ProfileDataContainer = styled.div`
  padding-left: 25px;
  padding-bottom: 25px;
`;

const AvatarRoot = styled.div`
  width: 160px;
  height: 160px;
  overflow: hidden;
  margin-top: -104px;
  border-radius: 50%;
  position: relative;
  background-color: ${White};
  border: solid 4px ${StoneWhite};
`;

const UploadAvatar = styled.div`
  top: 0;
  left: 0;
  opacity: 0;
  z-index: 20;
  width: 100%;
  height: 100%;
  display: flex;
  color: ${White};
  cursor: pointer;
  font-size: 26px;
  position: absolute;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  background-color: ${ModalColor};
  transition: opacity 0.1s ease-out;
  :hover {
    opacity: 1;
  }
`;

const Badge = styled.div`
  margin-top: 10px;
`;

const H1 = styled.h1`
  line-height: 32px;
  font-size: 24px;
  color: ${Black};
  margin: 0;
`;

const H2 = styled.h2`
  font-weight: normal;
  line-height: 24px;
  font-size: 18px;
  color: ${Black};
  margin: 0;
`;

const ProfileCard = props => {
  const { data, isMe } = props;
  const { name, desc } = data;

  return (
    <ProfileCardRoot>
      <ProfileBgContainer />
      <ProfileDataContainer>
        <AvatarRoot>
          {isMe && (
            <UploadAvatar onClick={props.onClickUpload}>
              <FaCamera />
            </UploadAvatar>
          )}
          <Avatar data={data} asLink={false} />
        </AvatarRoot>
        <Badge>
          <H1>{name}</H1>
        </Badge>
        <Badge>
          <H2>{desc}</H2>
        </Badge>
      </ProfileDataContainer>
    </ProfileCardRoot>
  );
};

ProfileCard.propTypes = {
  isMe: PropTypes.bool.isRequired,
  onClickUpload: PropTypes.func.isRequired,
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired
  }).isRequired
};

export default ProfileCard;
