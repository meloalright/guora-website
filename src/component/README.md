# component

## Quick Create

```js
import React, { Component } from 'react';
import styled from 'styled-components';

const DemoComponent = styled.div``;

const DemoComponent = () => {
  return <DemoComponentRoot>Demo</DemoComponentRoot>;
};

export default DemoComponent;
```

## 🍋 Components

`Post Content`

[BasePageCombination](./Combinations/PageCombination/BasePageCombination.js)

[AnswerQuestionCard](./AnswerQuestionCard.js)

[CommentCard](./TextCards/CommentCard.js)

[ReplyCard](./TextCards/ReplyCard.js)

`List Load More`

[QuestionList](./Lists/QuestionList/index.js)

[AnswerList](./Lists/AnswerList/index.js)

[CommentList](./Lists/CommentList/index.js)

[ReplyList](./Lists/ReplyList/index.js)

`Social Request`

[AnswerSocialBar](./SocialBars/AnswerSocialBar.js)

[CommentSocialBar](./SocialBars/CommentSocialBar.js)

[ReplySocialBar](./SocialBars/ReplySocialBar.js)

`Bubble`

[ProfileBubble](./Bubbles/ProfileBubble/index.js)

`Login`

[Login](./Logins/Login.js)

`Admin`

[AdministratorCreateUserModal](./Modals/AdministratorModals/AdministratorCreateUserModal.js)

[AdministratorEditModal](./Modals/AdministratorModals/AdministratorEditModal.js)

## 🐝 Components

[AskModal](./Modals/AskModal/index.js)

[AnswerQuestionCard](./AnswerQuestionCard.js)

[TextCard](./TextCards/TextCard.js)

[PopupCard](./TextCards/PopupCard.js)

[MainNavigator](./Navigators/MainNavigator.js)

[ProfileBubble](./Bubbles/ProfileBubble/index.js)

## 🕗 Components

[Avatar](./Avatar.js)

## Tips

```shell
1. 尽量宽度自适应撑开 由更外层容器框住宽度
```
