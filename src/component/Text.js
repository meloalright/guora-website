import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Black, LinkBlue, HoverBlack } from 'style/Color';

const P = styled.p`
  color: ${Black};
  font-size: 13px;
  line-height: 1.4;
  word-break: break-all;
`;

const Container = styled.div``;

const Collapsed = styled(Container)`
  cursor: pointer;
`;

const More = styled.span`
  margin: 0;
  color: ${LinkBlue};
  font-size: 13px;
  line-height: 1.4;
  word-break: break-word;
  :hover {
    color: ${HoverBlack};
  }
`;

class Text extends Component {
  constructor(props) {
    super(props);
    this.state = { collapse: this.props.children.length > 70 };
    this.expand = this.expand.bind(this);
  }

  expand() {
    const collapse = false;
    this.setState({ collapse });
  }

  render() {
    const { collapse } = this.state;

    return collapse ? (
      <Collapsed>
        <P onClick={this.expand}>
          {this.props.children.slice(0, 70)}
          ..
          <More onClick={this.expand}>(View More)</More>
        </P>
      </Collapsed>
    ) : (
      <Container>
        <P {...this.props}>{this.props.children}</P>
      </Container>
    );
  }
}

Text.propTypes = {
  children: PropTypes.node.isRequired
};

export default Text;
