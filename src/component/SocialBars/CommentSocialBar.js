import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FaReply } from 'react-icons/fa';
import SmallBadge from './SmallBadge';

const CommentSocialBarRoot = styled.div`
  display: flex;
  justify-content: left;
`;

class CommentSocialBar extends PureComponent {
  render() {
    const { data, onClickReply, onWatchReplies } = this.props;

    return (
      <CommentSocialBarRoot>
        <SmallBadge IconComponent={<FaReply />} onClick={onClickReply}>
          <FormattedMessage id="reply" />
        </SmallBadge>
        {data.repliesCounts > 0 && (
          <SmallBadge IconComponent={null} onClick={onWatchReplies}>
            <FormattedMessage id="replies.all" />({data.repliesCounts})
          </SmallBadge>
        )}
      </CommentSocialBarRoot>
    );
  }
}

CommentSocialBar.defaultProps = {
  onWatchReplies: null
};

CommentSocialBar.propTypes = {
  data: PropTypes.shape({
    repliesCounts: PropTypes.number.isRequired
  }).isRequired,
  onWatchReplies: PropTypes.func.isRequired,
  onClickReply: PropTypes.func.isRequired
};

export default CommentSocialBar;
