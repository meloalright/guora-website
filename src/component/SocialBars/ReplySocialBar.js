import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FaReply } from 'react-icons/fa';
import SmallBadge from './SmallBadge';

const ReplySocialBarRoot = styled.div`
  display: flex;
  justify-content: left;
`;

class ReplySocialBar extends PureComponent {
  render() {
    const { onClickReply } = this.props;

    return (
      <ReplySocialBarRoot>
        <SmallBadge IconComponent={<FaReply />} onClick={onClickReply}>
          <FormattedMessage id="reply" />
        </SmallBadge>
      </ReplySocialBarRoot>
    );
  }
}

ReplySocialBar.propTypes = {
  onClickReply: PropTypes.func.isRequired
};

export default ReplySocialBar;
