import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Blue, HoverBlue, Gray, GrayLight1 } from 'style/Color';

const SocialItem = styled.div`
  height: 33px;
  display: flex;
  cursor: pointer;
  font-size: 13px;
  user-select: none;
  line-height: 33px;
  margin-right: 11px;
  align-items: center;
  padding: 2px 5px 2px 0;
  box-sizing: border-box;
  justify-content: center;
  ${props =>
    props.active
      ? `
  color: ${Blue};
  :hover {
    color: ${HoverBlue};
  }
  `
      : `
  color: ${GrayLight1};
  :hover {
    color: ${Gray};
  }
  `}
`;

const IconRoot = styled.div`
  color: inherit;
  font-size: 13px;
  align-items: center;
  display: inline-flex;
`;

const Span = styled.span`
  color: inherit;
  font-size: 13px;
  margin-left: 5px;
  line-height: 33px;
  display: inline-block;
`;

const SocialSpan = styled(Span)`
  margin-left: 5px;
`;

const MediumBadge = props => (
  <SocialItem {...props} active={props.active}>
    <IconRoot>{props.IconComponent}</IconRoot>
    <SocialSpan>{props.children}</SocialSpan>
  </SocialItem>
);

MediumBadge.defaultProps = {
  active: false
};

MediumBadge.propTypes = {
  IconComponent: PropTypes.element.isRequired,
  children: PropTypes.node.isRequired,
  active: PropTypes.bool
};

export default MediumBadge;
