import styled from 'styled-components';
import { HoverBlue, Black } from 'style/Color';
import { Bold } from 'style/Weight';

const A = styled.a`
  text-decoration: none;
  line-height: 29px;
  font-weight: ${Bold};
  font-size: 18px;
  color: ${Black};
  margin: 0;
  :hover {
    color: ${HoverBlue};
  }
`;

const LinkTitle = A;

export default LinkTitle;
