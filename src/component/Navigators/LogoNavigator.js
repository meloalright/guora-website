import React from 'react';
import styled from 'styled-components';

import { ContentColumn } from 'style/Layout';
import Image from 'widget/Image';
import Navigator from './Navigator';

const Root = styled.div`
  height: 100%;
  display: flex;
  margin: 0 auto;
  padding-left: 20px;
  align-items: center;
  max-width: ${ContentColumn};
  justify-content: space-between;
`;

const LogoContainer = styled.a`
  display: inline-block;
  width: 93px;
`;

const LogoNavigator = () => {
  return (
    <Navigator>
      <Root>
        <LogoContainer href="/">
          <Image src="/static/assets/guora.svg" />
        </LogoContainer>
      </Root>
    </Navigator>
  );
};

LogoNavigator.propTypes = {};
export default LogoNavigator;
