import styled from 'styled-components';
import { White } from 'style/Color';

const Navigator = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 52px;
  z-index: 800;
  position: fixed;
  background-color: ${White};
  border-bottom: 1px solid #ddd;
  box-shadow: 0 3px 2px -2px rgba(200, 200, 200, 0.2);
`;

export default Navigator;
