import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import { ContentColumn } from 'style/Layout';
import MeService from 'service/MeService';

import ProfileBubble from 'component/Bubbles/ProfileBubble';

import Button from 'widget/Button';
import Image from 'widget/Image';
import Navigator from './Navigator';

const Root = styled.div`
  height: 100%;
  display: flex;
  margin: 0 auto;
  padding-left: 20px;
  align-items: center;
  max-width: ${ContentColumn};
  justify-content: space-between;
`;

const LogoContainer = styled.a`
  display: inline-block;
  width: 93px;
`;

const AvatarContainer = styled.div`
  width: 26px;
  height: 26px;
  cursor: pointer;
  overflow: hidden;
  border-radius: 50%;
`;

const Holder = styled.div`
  flex: 1;
`;

const Padding = styled.div`
  position: relative;
  padding: 0 10px;
`;

const ProfileBubbleContainer = styled.div`
  position: absolute;
  outline: none;
  border: none;
  right: 10px;
  opacity: 0;
  top: 30px;
  height: 0;
  width: 0;
  :focus {
    min-height: 240px;
    width: 240px;
    opacity: 1;
  }
`;

const MainNavigator = props => {
  const bubbleRef = React.createRef();
  const handleClick = () => {
    bubbleRef.current.focus();
  };
  return (
    <Navigator>
      <Root>
        <LogoContainer href="/">
          <Image src="/static/assets/guora.svg" />
        </LogoContainer>
        <Holder />
        <Padding onClick={handleClick}>
          <AvatarContainer>
            {MeService.AvatarComponent({
              asLink: false
            })}
          </AvatarContainer>
          <ProfileBubbleContainer tabIndex={0} ref={bubbleRef}>
            <ProfileBubble />
          </ProfileBubbleContainer>
        </Padding>
        <Padding>
          <Button theme="primary" bold onClick={props.onClickAsk}>
            <FormattedMessage id="navigator.post" />
          </Button>
        </Padding>
      </Root>
    </Navigator>
  );
};

MainNavigator.propTypes = {
  onClickAsk: PropTypes.func.isRequired
};
export default MainNavigator;
