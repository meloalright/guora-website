import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from 'widget/Modal';
import { IoMdCloseCircleOutline } from 'react-icons/io';
import { White } from 'style/Color';

const ModalContainer = styled.div`
  position: relative;
  margin: auto;
  width: 620px;
`;

const Close = styled(IoMdCloseCircleOutline)`
  position: absolute;
  cursor: pointer;
  color: ${White};
  padding: 12px;
  height: 24px;
  right: -48px;
  width: 24px;
  top: 0;
`;

const ModalColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const Wrap = styled.div`
  min-height: 130px;
  border-radius: 2px;
  box-sizing: border-box;
  background-color: ${White};
  padding: 32px 32px 24px 32px;
`;

const BaseModal = props => {
  return (
    <Modal>
      <ModalContainer>
        <Close onClick={props.onClose} />
        <ModalColumn>
          <Wrap>{props.children}</Wrap>
        </ModalColumn>
      </ModalContainer>
    </Modal>
  );
};

BaseModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired
};

export default BaseModal;
