import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Button from 'widget/Button';
import { H3 } from 'widget/PageHead';
import LoadingPage from 'widget/LoadingPage';
import BaseModal from 'component/Modals/BaseModal';
import SignService from 'service/RequestServices/SignService';
import AdministratorBaseContent from './AdministratorBaseContent';

const Labels = [
  {
    label: 'name',
    disabled: false,
    type: 'string',
    value: ''
  },
  {
    label: 'mail',
    disabled: false,
    type: 'string',
    value: ''
  },
  {
    label: 'password',
    disabled: false,
    type: 'string',
    value: ''
  }
];

class AdministratorCreateUserModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      labels: Labels
    };
    this.modalRef = React.createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    let { loading } = this.state;
    const form = this.modalRef.current.getForm();
    loading = true;
    this.setState({ loading });

    SignService.sign(form).then(data => {
      console.log('data', data);
      location.href = location.href;
    });
  }

  render() {
    const { loading, labels } = this.state;
    return (
      <BaseModal onClose={this.props.onClose}>
        {loading ? (
          <LoadingPage />
        ) : (
          <AdministratorBaseContent
            labels={labels}
            ref={this.modalRef}
            header={
              <H3>
                <FormattedMessage id="admin.create.user" />
              </H3>
            }
            onSubmit={this.handleSubmit}
            footer={
              <Button theme="primary" bold onClick={this.handleSubmit}>
                <FormattedMessage id="submit2" />
              </Button>
            }
          />
        )}
      </BaseModal>
    );
  }
}

AdministratorCreateUserModal.propTypes = {
  onClose: PropTypes.func.isRequired
};

export default AdministratorCreateUserModal;
