/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import BaseModal from 'component/Modals/BaseModal';
import Button from 'widget/Button';
import TextArea from 'widget/TextArea';
import MeService from 'service/MeService';
import RichTextEditor from 'component/RichTexts/RichTextEditor';
import LoadingPage from 'widget/LoadingPage';
import { GrayLight1 } from 'style/Color';
import { Boldest } from 'style/Weight';

const Badge = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const HeadBadge = styled(Badge)`
  display: flex;
  margin-bottom: 16px;
`;

const AskTextArea = styled(TextArea)`
  font-weight: ${Boldest};
  margin-left: 16px;
  line-height: 1.3;
  font-size: 18px;
  padding: 0;
  flex: 1;
`;

const AvatarContainer = styled.div`
  width: 40px;
  height: 40px;
  overflow: hidden;
  border-radius: 50%;
`;

const FooterBadge = styled.div`
  height: 56px;
  padding: 10px 0;
  text-align: right;
`;

const Privacy = styled.div`
  display: inline-flex;
  align-items: center;
  color: ${GrayLight1};
  font-size: 15px;
  cursor: pointer;
`;

const CheckBox = styled.input`
  margin-right: 8px;
`;

class AskModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      title: ''
    };
    this.inputRef = React.createRef();
    this.editorRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.inputRef.current.focus();
  }

  handleChange(event) {
    this.setState({ title: event.target.value });
  }

  handleBlur() {
    let { title } = this.state;
    if (title.length > 0 && !title.endsWith('?') && !title.endsWith('？')) {
      title += '?';
    }
    this.inputRef.current.value = title;
    this.setState({ title });
  }

  handleClick() {
    let { loading } = this.state;
    loading = true;
    const { title } = this.state;
    const desc = JSON.stringify(this.editorRef.current.getRaw());
    this.setState(
      {
        loading
      },
      () => {
        this.props.onSubmit({
          title,
          desc
        });
      }
    );
  }

  render() {
    const { title, loading } = this.state;
    if (loading) {
      return <LoadingPage />;
    }

    const isValid = title.length > 0;
    return (
      <BaseModal onClose={this.props.onClose}>
        <HeadBadge>
          <AvatarContainer>{MeService.AvatarComponent()}</AvatarContainer>
          <FormattedMessage id="post.question.placeholder">
            {([txt]) => (
              <AskTextArea
                ref={this.inputRef}
                placeholder={txt}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
              />
            )}
          </FormattedMessage>
        </HeadBadge>
        {title.length > 0 && <RichTextEditor ref={this.editorRef} type="question" />}
        <FooterBadge>
          <Button theme="primary" disabled={!isValid} bold onClick={this.handleClick}>
            <FormattedMessage id="post.question" />
          </Button>
        </FooterBadge>
      </BaseModal>
    );
  }
}

AskModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default AskModal;
