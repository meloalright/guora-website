import styled from 'styled-components';
import { Black } from 'style/Color';
import { Bold } from 'style/Weight';

const PageHead = h => styled(h)`
  font-weight: ${Bold};
  color: ${Black};
  margin: 0;
`;

export const H2 = PageHead(styled.h2``);
export const H3 = PageHead(styled.h3``);
export const H4 = PageHead(styled.h4``);
