import styled from 'styled-components';

const Light = styled.section`
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
`;

export default Light;
