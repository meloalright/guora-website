import React from 'react';
import styled from 'styled-components';
import ReactLoading from 'react-loading';
import { Blue } from 'style/Color';

const LoadingContainer = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  position: absolute;
  text-align: center;
  align-items: center;
`;

const Loading = styled(ReactLoading)`
  margin: auto;
`;

const LoadingPage = () => {
  return (
    <LoadingContainer>
      <Loading type="bubbles" color={Blue} width={40} height={40} />
    </LoadingContainer>
  );
};

export default LoadingPage;
