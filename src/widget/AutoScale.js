import styled from 'styled-components';

const AutoScale = styled.div`
  animation: 0.3s ease-out autoscale;
`;

export default AutoScale;
