import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from './Button';

const FaSpan = styled.span`
  font-size: 17px;
  margin-right: 8px;
  align-items: center;
  display: inline-flex;
`;

const FaButton = ({ fa, children, ...props }) => (
  <Button {...props}>
    <FaSpan>{fa}</FaSpan>
    {children}
  </Button>
);

FaButton.propTypes = {
  fa: PropTypes.element.isRequired,
  children: PropTypes.node.isRequired
};

export default FaButton;
