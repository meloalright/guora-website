import styled from 'styled-components';
import { White } from 'style/Color';
import Light from './Light';

const Card = styled(Light)`
  box-shadow: 0 0 0 1px rgba(26, 26, 26, 0.1), 0 2px 3px rgba(26, 26, 26, 0.12);
  background-color: ${White};
  border-radius: 1px;
  overflow: hidden;
`;

export default Card;
