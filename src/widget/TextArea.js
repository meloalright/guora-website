import styled from 'styled-components';
import TextareaAutosize from 'react-autosize-textarea';
import { Black } from 'style/Color';

const TextArea = styled(TextareaAutosize)`
  border: none;
  resize: none;
  color: ${Black};
  background: transparent;
`;

export default TextArea;
