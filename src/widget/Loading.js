import React from 'react';
import { GrayLight3 } from 'style/Color';
import ReactLoading from 'react-loading';
import styled from 'styled-components';

const Root = styled.div`
  text-align: center;
`;

const LoadingBar = styled(ReactLoading)`
  margin: auto;
`;

const Loading = () => (
  <Root>
    <LoadingBar type="bubbles" color={GrayLight3} width={40} height={40} />
  </Root>
);

export default Loading;
