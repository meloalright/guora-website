import styled from 'styled-components';
import {
  White,
  Blue,
  HoverBlue,
  BrightBlue,
  SkyBlue,
  HoverSkyBlue,
  HoverBrightBlue,
  Transparent,
  HoverTransparent,
  Red,
  LightRed,
  HoverLightRed
} from 'style/Color';
import { Bold } from 'style/Weight';

// cssTmpl
const cssTmpl = Colors => `
    color: ${Colors[0]};
    background-color: ${Colors[1]};
    border: 1px solid ${Colors[2]};
    ${props =>
      props.disabled
        ? ''
        : `
    :hover {
        background-color: ${Colors[3]};
    }`}
`;

const Button = styled.button`
  cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  font-weight: ${props => (props.bold ? Bold : 'normal')};
  opacity: ${props => (props.disabled ? 0.7 : 1)};
  display: inline-flex;
  align-items: center;
  border-radius: 3px;
  text-align: center;
  line-height: 32px;
  padding: 0 16px;
  font-size: 14px;
  ${props =>
    cssTmpl(
      {
        normal: [Blue, SkyBlue, SkyBlue, HoverSkyBlue],
        primary: [White, Blue, Blue, HoverBlue],
        info: [White, BrightBlue, BrightBlue, HoverBrightBlue],
        transparent: [Blue, Transparent, Transparent, HoverTransparent],
        danger: [Red, LightRed, LightRed, HoverLightRed]
      }[props.theme]
    )}
`;

export default Button;
