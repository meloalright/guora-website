import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Root = styled.img`
  transition: opacity 0.3s ease-out;
  height: 100%;
  width: 100%;
  opacity: ${props => (props.transparent ? 0 : 1)};
`;

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transparent: true
    };
    this.fadeIn = this.fadeIn.bind(this);
  }

  fadeIn() {
    const transparent = false;
    this.setState({ transparent });
  }

  render() {
    const { transparent } = this.state;
    const { src, ...props } = this.props;

    return <Root src={src} {...props} transparent={transparent} onLoad={this.fadeIn} />;
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired
};

export default Image;
