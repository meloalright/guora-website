import styled from 'styled-components';
import { LinkBlue } from 'style/Color';

const A = styled.a`
  word-break: break-word;
  text-decoration: none;
  color: ${LinkBlue};
  line-height: 20px;
  cursor: pointer;
  font-size: 14px;
  margin: 0;
  :hover {
    text-decoration: underline;
  }
`;

const Link = A;

export default Link;
