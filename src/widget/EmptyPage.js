import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Gray } from 'style/Color';

const EmptyContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  align-items: center;
  height: 100%;
  display: flex;
`;

const EmptyText = styled.p`
  text-align: center;
  line-height: 1.4;
  font-size: 13px;
  color: ${Gray};
  flex: 1;
`;

const EmptyPage = props => {
  return (
    <EmptyContainer>
      <EmptyText>{props.children}</EmptyText>
    </EmptyContainer>
  );
};

EmptyPage.propTypes = {
  children: PropTypes.node.isRequired
};

export default EmptyPage;
