import styled from 'styled-components';
import { ModalColor } from 'style/Color';

const Modal = styled.div`
  animation: fadeIn 100ms ease-out 0s 1 normal forwards;
  background: ${ModalColor};
  position: fixed;
  overflow-y: scroll;
  overflow-x: auto;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1400;
  display: flex;
  align-items: center;
`;

export default Modal;
