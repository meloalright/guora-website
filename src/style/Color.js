export const Blue = '#1e78f3';
export const HoverBlue = '#1771ed';

export const LinkBlue = '#2b6dad';

export const BrightBlue = '#5890ff';
export const HoverBrightBlue = '#5289f7';

export const SkyBlue = '#F0F8FF';
export const HoverSkyBlue = '#F3FBFF';

export const ModalColor = 'rgba(0, 0, 0, 0.65)';

export const Black = '#333333';
export const HoverBlack = '#646464';

export const Red = '#df4930';
export const LightRed = '#FCF2F2';
export const HoverLightRed = '#FFF5F5';

export const StoneWhite = '#EFEFEF';

export const ShadowWhite = '#FAFAFA';

export const AlmostWhite = '#FCFCFC';

export const White = '#FFFFFF';
export const HoverWhite = 'rgba(0, 132, 255, 0.06)';

export const Gray = '#646464';
export const GrayLight1 = '#767676';
export const GrayLight2 = '#999999';
export const GrayLight3 = '#CCCCCC';
export const GrayLight4 = '#EEEEEE';

export const Transparent = 'transparent';
export const HoverTransparent = 'rgba(0, 0, 0, 0.02)';
