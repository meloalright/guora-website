import styled from 'styled-components';
import { ContentColumn, MainColumn, SideColumn } from 'style/Layout';

export const DashboardRoot = styled.div`
  width: 100%;
`;

export const HeadColumnRoot = styled.div`
  padding: 83px 0 12px 0;
  box-sizing: border-box;
  width: ${ContentColumn};
`;

export const BodyColumnRoot = styled.div`
  justify-content: space-between;
  align-items: flex-start;
  box-sizing: border-box;
  width: ${ContentColumn};
  display: flex;
  padding: 0;
`;

export const SideColumnRoot = styled.div`
  box-sizing: border-box;
  padding: 0 12px 0 0;
  width: ${SideColumn};
`;

export const MainColumnRoot = styled.div`
  box-sizing: border-box;
  width: ${MainColumn};
  padding: 0;
`;
