import styled from 'styled-components';
import { MainColumn, SideColumn } from 'style/Layout';

export const DashboardRoot = styled.div`
  justify-content: space-between;
  align-items: flex-start;
  display: flex;
  width: 100%;
`;

export const MainColumnRoot = styled.div`
  padding: 83px 32px 32px 32px;
  box-sizing: border-box;
  width: ${MainColumn};
`;

export const SideColumnRoot = styled.div`
  padding: 83px 0 32px 0;
  box-sizing: border-box;
  width: ${SideColumn};
`;
