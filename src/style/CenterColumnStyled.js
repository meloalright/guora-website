import styled from 'styled-components';
import { ContentColumn } from 'style/Layout';

export const DashboardRoot = styled.div`
  width: 100%;
`;

export const CenterColumnRoot = styled.div`
  padding: 83px 32px 32px 32px;
  box-sizing: border-box;
  width: ${ContentColumn};
`;
