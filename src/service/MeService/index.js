import React from 'react';
import Avatar from 'component/Avatar';
import cookie from 'library/cookie';

const ss = decodeURIComponent(cookie.getCookie('ss'));

function parseJwt(token) {
  try {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  } catch (e) {
    console.log('[parseJwt]: e', e);
    return e;
  }
}

const ssClaims = parseJwt(ss);

const ssName = decodeURIComponent(ssClaims['profile.name']);
const ssDesc = decodeURIComponent(ssClaims['profile.desc']);

export default {
  AvatarComponent: attr => <Avatar data={{ id: ssClaims.id }} {...attr} />,
  GetMe: () => ({ id: ssClaims.id, name: ssName, type: ssClaims.type, desc: ssDesc })
};
