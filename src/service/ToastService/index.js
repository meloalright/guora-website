import React from 'react';
import styled from 'styled-components';
import { toast, Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const P = styled.p`
  text-align: center;
`;

const init = () => {
  toast.configure({
    position: 'top-center',
    autoClose: 1500,
    transition: Flip,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true
  });
};

const normal = msg => toast.default(<P>{msg}</P>);

const error = msg => toast.error(<P>{msg}</P>);

export default {
  init,
  normal,
  error
};
