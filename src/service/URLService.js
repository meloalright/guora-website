/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/**

Usage:

import { URL } from 'service/URLService';

let link = URL.profile(${id})

*/

export const HTML = {
  index: `/`,
  profile: `/profile`,
  question: `/question`,
  answer: `/answer`,
  admin: `/admin`,
  error: `/error`,
  login: `/login`
};

const search = new URLSearchParams(location.search);

export const URL = {
  index: () => `${HTML.index}`,
  profile: id => `${HTML.profile}?id=${id}`,
  question: id => `${HTML.question}?id=${id}`,
  answer: id => `${HTML.answer}?id=${id}`,
  admin: () => `${HTML.admin}`,
  error: message => `${HTML.error}?message=${message}`,
  login: () => `${HTML.login}`,
  avatar: id => `/static/avatar/${id}.png`,

  search: key => search.get(key),
  go: url => {
    location.href = url;
  },
  back: () => {
    history.back(-1);
  }
};

export const Domain = {
  official: ``
};
