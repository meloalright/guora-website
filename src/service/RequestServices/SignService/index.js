import RequestService from 'service/RequestServices/base';

const sign = data => {
  return RequestService.Post(`/api/web/security/sign`, data);
};

export default { sign };
