import RequestService from 'service/RequestServices/base';

const postQuestion = ({ title, desc }) => {
  return RequestService.Post(`/api/web/question`, { title, desc });
};

export default {
  postQuestion
};
