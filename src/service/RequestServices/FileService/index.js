import RequestService from 'service/RequestServices/base';

const uploadAvatar = form => {
  return RequestService.PostForm(`/api/web/file/avatar`, form);
};

export default {
  uploadAvatar
};
