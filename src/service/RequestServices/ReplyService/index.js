import RequestService from 'service/RequestServices/base';

const fetchReplies = (id, limit, offset) => {
  return RequestService.Get(`/api/web/comment/${id}/replies?offset=${offset}&limit=${limit}`);
};

const postReply = ({ commentID, content, type, replyToProfileID }) => {
  return RequestService.Post(`/api/web/reply`, {
    commentID,
    content,
    type,
    replyToProfileID
  });
};

const postReplySupporter = id => {
  return RequestService.Post(`/api/web/reply/${id}/supporters`, {});
};

const deleteReplySupporter = id => {
  return RequestService.Delete(`/api/web/reply/${id}/supporters`);
};

export default {
  fetchReplies,
  postReply,
  postReplySupporter,
  deleteReplySupporter
};
