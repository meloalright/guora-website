import ToastService from 'service/ToastService';

const CatchMethod = func => (...args) =>
  new Promise((resolve, reject) => {
    func
      .apply(this, args)
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        const message = typeof e.message === 'string' ? e.message : `ops! something wrong`;
        ToastService.error(message);
        reject(e);
      });
  });

export default CatchMethod;
