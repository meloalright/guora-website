import RequestService from 'service/RequestServices/base';

const Tables = {
  users: { orm: 'user' },
  profiles: { orm: 'profile' },
  questions: { orm: 'question' },
  answers: { orm: 'answer' },
  comments: { orm: 'comment' },
  replies: { orm: 'reply' }
};

const fetchRest = (table, id) => {
  const orm = Tables[table].orm;
  return RequestService.Get(`/api/rest/${orm}/${id}`);
};

// const postRest = (table, id, data) => {
//   const orm = Tables[table].orm;
//   return RequestService.Post(`/api/rest/${orm}/${id}`, data);
// };

// const putRest = (table, id, data) => {
//   const orm = Tables[table].orm;
//   return RequestService.Put(`/api/rest/${orm}/${id}`, data);
// };

const deleteRest = (table, id) => {
  const orm = Tables[table].orm;
  return RequestService.Delete(`/api/rest/${orm}/${id}`);
};

const fetchRestList = (table, limit, offset, suffix = null) => {
  return RequestService.Get(
    `/api/rest/${table}?offset=${offset}&limit=${limit}${suffix ? `&${suffix}` : ''}`
  );
};

const fetchRestCounts = table => {
  return RequestService.Get(`/api/rest/${table}/counts`);
};

export default {
  fetchRest,
  // postRest,
  // putRest,
  deleteRest,
  fetchRestList,
  fetchRestCounts,
  Tables
};
