const profile = {
  id: 1,
  createAt: 1596860045,
  updateAt: 1596860045,
  name: 'Guora Robot(小助手)',
  desc: 'This is Guora Robot(小助手)'
};

const question = {
  id: 1,
  createAt: 1596860045,
  updateAt: 1596860045,
  title: '这玩意咋用？',
  desc:
    '{"blocks":[{"key":"bvelr","text":"这个 Guora 究竟如何使用？","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
  type: 0,
  questionProfile: profile,
  questionProfileID: 1,
  answersCounts: 2
};

const answer = {
  id: 1,
  createAt: 1596862148,
  updateAt: 1596862111,
  content:
    '{"blocks":[{"key":"dpmcj","text":"Hello everyone, this is an example of answer.","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":62,"length":0,"style":"ITALIC"}],"entityRanges":[{"offset":62,"length":17,"key":0}],"data":{}},{"key":"fc5fr","text":"First of all, Guora is a self hosted web application based on community Q \u0026 A.","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"csb7o","text":"When you host the application, you can enjoy the Q \u0026 A platform and connect with your working partners. You can also archive your documents here.","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"eubjo","text":"If you\'re the administrator, you can go to the Admin Page to manage the application.","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":47,"length":10,"style":"ITALIC"}],"entityRanges":[{"offset":47,"length":10,"key":1}],"data":{}}],"entityMap":{"0":{"type":"LINK","mutability":"MUTABLE","data":{"href":"/question?id=1","title":"/question?id=1","url":"/question?id=1"}},"1":{"type":"LINK","mutability":"MUTABLE","data":{"href":"/admin","title":"/admin","url":"/admin"}}}}',
  type: -1,
  question,
  questionID: 1,
  answerProfile: profile,
  answerProfileID: 1,
  commentsCounts: 2,
  supportersCounts: 0,
  supported: false
};

const hotAnswers = Array(10)
  .fill(0)
  .map((_, key) => ({
    ...answer,
    id: key + 1
  }));

const hotQuestions = Array(10)
  .fill(0)
  .map((_, key) => ({
    ...question,
    id: key + 1
  }));

const profileAnswers = Array(10)
  .fill(0)
  .map((_, key) => ({
    ...answer,
    id: key + 1
  }));

const profileQuestions = Array(10)
  .fill(0)
  .map((_, key) => ({
    ...question,
    id: key + 1
  }));

const questionAnswers = Array(10)
  .fill(0)
  .map((_, key) => ({
    ...answer,
    id: key + 1
  }));

const lang = 'zh';

module.exports = {
  lang,
  index: {
    hotAnswers,
    hotAnswersCounts: 4,
    hotQuestions
  },
  profile: {
    profile,
    answers: profileAnswers,
    answersCounts: 20,
    questions: profileQuestions,
    questionsCounts: 20,
    hotQuestions
  },
  question: {
    question,
    answers: questionAnswers,
    answersCounts: 20,
    hotQuestions
  },
  answer: {
    answer,
    hotQuestions
  },
  admin: {},
  login: {},
  error: {}
};
