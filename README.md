# Guora Webs

## Quick Start

```shell
$ yarn
```

```shell
$ yarn start
```

## Lint

```shell
$ yarn lint
```

## Pretty

```shell
$ yarn pretty
```

## Component

[Component README](./src/component/README.md)
